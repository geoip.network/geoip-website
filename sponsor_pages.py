from base64 import b64encode
from io import BytesIO
from os import environ
from os.path import exists, join as file_join
from urllib.parse import urljoin
from uuid import UUID

import magic
import requests
import stripe
from flask import Blueprint, render_template, session, current_app, request, url_for
from wand.image import Image
from werkzeug.utils import secure_filename, redirect

import api_manager
import auth0_userdata
import tracking_manager
from auth_decorators import requires_auth
from auth_manager import get_apikeys, create_apikey, delete_apikey
from helpers import calculate_totals, calculate_statistics, generate_chart
from page_render_helpers import build_map, render_funding_progress
from security_helpers import url_safe_check
from stripe_wrapper import (
    get_subscription,
    get_plan_details,
    requires_subscription,
    new_subscription,
    update_subscription,
    get_or_create_customer,
    get_clean_pricing,
)
from tracking_wrapper import get_tracked_ips


bp = Blueprint("sponsor", __name__, template_folder="templates", url_prefix="/auth")


@bp.route("/")
@requires_auth
@requires_subscription
def dashboard(current_subscription):
    user_id = session["profile"]["user_id"]
    logo_b64 = False
    logo_path = file_join(
        current_app.config["SPONSOR_UPLOAD_FOLDER"], secure_filename(f"{user_id}.png")
    )
    if exists(logo_path):
        with open(logo_path, "rb") as f:
            logo = f.read()
            logo_b64 = b64encode(logo).decode()
    return render_template(
        "sponsor_dashboard.html",
        funding_progress=render_funding_progress(26.69, 3500),
        selected_page="dashboard",
        current_tier=current_subscription[1],
        logo_b64=logo_b64,
        logout=True,
    )


@bp.route("/map")
@requires_auth
@requires_subscription
def map(current_subscription):
    geoip_records = api_manager.lookup_cidrs(get_tracked_ips())
    rendered_map = build_map(geoip_records)
    return rendered_map, 200


@bp.route("/apikeys", methods=["GET"])
@requires_auth
@requires_subscription
def apikeys(current_subscription):
    api_keys = get_apikeys(session["profile"]["user_id"])
    tracked_keys = {
        api_key["application_name"]: tracking_manager.get_token(
            session["profile"]["user_id"], api_key["application_name"]
        )
        for api_key in api_keys
    }
    return render_template(
        "sponsor_apikeys.html",
        selected_page="api",
        api_keys=api_keys,
        tracked_keys=tracked_keys,
        logout=True,
    )


@bp.route("/apikeys", methods=["POST"])
@requires_auth
@requires_subscription
def apikeys_post(current_subscription):
    application_name = request.form.get("name")
    is_bearer = request.form["type"] == "bearer"
    track = request.form.get("tracking", "off") == "on"
    new_key = create_apikey(session["profile"]["user_id"], application_name, is_bearer)
    tracked = False
    if track:
        try:
            tracking_manager.create_token(
                session["profile"]["user_id"], application_name
            )
            tracked = True
        except requests.HTTPError:
            pass
    api_keys = get_apikeys(session["profile"]["user_id"])
    tracked_keys = {
        api_key["application_name"]: tracking_manager.get_token(
            session["profile"]["user_id"], api_key["application_name"]
        )
        for api_key in api_keys
    }
    return render_template(
        "sponsor_apikeys.html",
        selected="api",
        api_keys=api_keys,
        tracked_keys=tracked_keys,
        new_key=new_key,
        track=track,
        tracked=tracked,
        logout=True,
    )


@bp.route("/apikeys/<application_name>", methods=["GET"])
@requires_auth
@requires_subscription
def apikeys_metrics(application_name, current_subscription):
    api_keys = get_apikeys(session["profile"]["user_id"])
    visits = tracking_manager.get_token_visits(
        session["profile"]["user_id"], str(application_name)
    )
    geoip_records = api_manager.lookup_cidrs(
        tuple([visit["IPAddr"] for visit in visits])
    )
    totals_histograms = calculate_totals(visits, geoip_records)
    countries_data = calculate_statistics(totals_histograms, visits, geoip_records)
    visits_chart = generate_chart(totals_histograms)
    tracked_keys = {
        api_key["application_name"]: tracking_manager.get_token(
            session["profile"]["user_id"], api_key["application_name"]
        )
        for api_key in api_keys
    }
    return render_template(
        "sponsor_apikey_metrics.html",
        selected_page="api",
        api_keys=api_keys,
        tracked_keys=tracked_keys,
        table=countries_data,
        visits=visits,
        application_name=str(application_name),
        map=build_map(geoip_records),
        visits_chart=visits_chart,
        logout=True,
    )


@bp.route("/apikeys/delete", methods=["POST"])
@requires_auth
@requires_subscription
def apikeys_delete(current_subscription):
    is_bearer = request.form["type"].lower() == "bearer"
    application_name = request.form.get("application_name")
    key_id = request.form.get("key_id")
    delete_apikey(session["profile"]["user_id"], application_name, key_id, is_bearer)
    return redirect(url_for(".apikeys"))


@bp.route("/metrics/pixel", methods=["GET"])
@requires_auth
@requires_subscription
def pixel(current_subscription):
    pixels = tracking_manager.get_pixels(session["profile"]["user_id"])
    return render_template(
        "sponsor_pixel.html",
        selected_page="pixel",
        pixel_url=urljoin(
            environ.get("GEOIP_TRACKINGSERVICE_URL"), f"v1.0/pixel/visit"
        ),
        pixels=pixels,
        logout=True,
    )


@bp.route("/metrics/pixel", methods=["POST"])
@requires_auth
@requires_subscription
def pixel_post(current_subscription):
    tracking_manager.create_pixel(session["profile"]["user_id"])
    pixels = tracking_manager.get_pixels(session["profile"]["user_id"])
    return render_template(
        "sponsor_pixel.html",
        selected="pixel",
        pixel_url=urljoin(
            environ.get("GEOIP_TRACKINGSERVICE_URL"), f"v1.0/pixel/visit"
        ),
        pixels=pixels,
        logout=True,
    )


@bp.route("/metrics/pixel/delete", methods=["POST"])
@requires_auth
@requires_subscription
def pixel_delete(current_subscription):
    key_id = request.form.get("pixel_uuid")
    tracking_manager.delete_pixel(session["profile"]["user_id"], key_id)
    return redirect(url_for("pixel"))


@bp.route("/metrics/pixel/<uuid:key_id>", methods=["GET"])
@requires_auth
@requires_subscription
def pixel_metrics(key_id: UUID, current_subscription):
    pixels = tracking_manager.get_pixels(session["profile"]["user_id"])
    visits = tracking_manager.get_visits(session["profile"]["user_id"], str(key_id))
    geoip_records = api_manager.lookup_cidrs(
        tuple([visit["IPAddr"] for visit in visits])
    )
    totals_histograms = calculate_totals(visits, geoip_records)
    countries_df = calculate_statistics(totals_histograms, visits, geoip_records)
    visits_chart = generate_chart(totals_histograms)
    return render_template(
        "sponsor_pixel_metrics.html",
        selected_page="pixel",
        pixel_url=urljoin(
            environ.get("GEOIP_TRACKINGSERVICE_URL"), f"v1.0/pixel/visit"
        ),
        pixels=pixels,
        table=countries_df,
        visits=visits,
        uuid=str(key_id),
        map=build_map(geoip_records),
        visits_chart=visits_chart,
        logout=True,
    )


@bp.route("/account", methods=["GET"])
@requires_auth
def account():
    clean_pricing = get_clean_pricing()
    selected = clean_pricing[0][2]
    active = None
    current = None
    stripe_customer_id = session["meta"].get("stripe_customer_id")
    if stripe_customer_id is not None:
        active = get_subscription(session["profile"]["user_id"], stripe_customer_id)
        if active is not None:
            current = get_plan_details(active.latest_invoice)[1]
            selected = active["items"]["data"][0]["price"]["id"]
    return render_template(
        "sponsor_account.html",
        selected_page="account",
        selected=selected,
        pricing=clean_pricing,
        active=active,
        current=current,
        logout=True,
    )


@bp.route("/account", methods=["POST"])
@requires_auth
def account_post():
    selected = request.form.get("tier")
    stripe_customer_id = get_or_create_customer()
    clean_pricing = {price[2]: True for price in get_clean_pricing()}
    if selected not in clean_pricing:
        return redirect(url_for(".account"))
    active_subscription = get_subscription(
        session["profile"]["user_id"], stripe_customer_id
    )
    if active_subscription is None:
        return new_subscription(stripe_customer_id, selected)
    else:
        return update_subscription(active_subscription, selected)


@bp.route("/account/cancel", methods=["GET"])
@requires_auth
def account_cancel_no_pay():
    return redirect(url_for(".account"))


@bp.route("/account/cancel", methods=["POST"])
@requires_auth
@requires_subscription
def account_cancel(current_subscription):
    subscription_id = session["meta"]["stripe_subscription_id"]
    stripe.Subscription.modify(
        subscription_id,
        pause_collection=True,
        proration_behavior="none",
        cancel_at_period_end=True,
    )
    return redirect(url_for(".account"))


@bp.route("/logo", methods=["GET"])
@requires_auth
@requires_subscription
def logo(current_subscription):
    user_id = session["profile"]["user_id"]
    user = auth0_userdata.user_info(user_id)
    logo_b64 = False
    logo_path = file_join(
        current_app.config["SPONSOR_UPLOAD_FOLDER"], secure_filename(f"{user_id}.png")
    )
    if exists(logo_path):
        with open(logo_path, "rb") as f:
            logo = f.read()
            logo_b64 = b64encode(logo).decode()
    bad_file = request.args.get("bad_file", False)
    return render_template(
        "sponsor_logo.html",
        logo_b64=logo_b64,
        sponsor_url=user.get("app_metadata", {}).get("sponsor_url", ""),
        display_name=user.get("app_metadata", {}).get("display_name", ""),
        bad_file=bad_file,
        logout=True,
    )


@bp.route("/sponsor_logo", methods=["POST"])
@requires_auth
@requires_subscription
def sponsor_logo_upload(current_subscription):
    user_id = session["profile"]["user_id"]
    logo = request.files["logo"]
    if logo.filename != "":
        file_stream = BytesIO(logo.stream.read())
        file_stream.seek(0)
        with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
            mime = m.id_buffer(file_stream.read())
            file_stream.seek(0)
        if (mime != logo.mimetype) or (mime not in ["image/png", "image/jpeg"]):
            return redirect(url_for("logo") + "?bad_file=true")
        with Image(file=file_stream) as img:
            img.format = "png"
            img.transform(resize="530x277>")
            img.transform(resize="530x277<")
            img.save(
                filename=file_join(
                    current_app.config["SPONSOR_UPLOAD_FOLDER"],
                    secure_filename(f"{user_id}.png"),
                )
            )
    if display_name := request.form.get("display_name", False):
        auth0_userdata.update_user_meta(user_id, meta={"display_name": display_name})
    if (sponsor_url := request.form.get("sponsor_url", False)) and url_safe_check(
        sponsor_url
    ):
        auth0_userdata.update_user_meta(user_id, meta={"sponsor_url": sponsor_url})
    return redirect(url_for("logo"))


@bp.route("/password")
def password():
    return render_template("sponsor_password.html", logout=True)


@bp.route("/password", methods=["POST"])
def sponsor_password_post():
    user_id = session["profile"]["user_id"]
    user = auth0_userdata.user_info(user_id)
    jdata = {
        "client_id": environ.get("GEOIP_AUTH_ID"),
        "email": user.get("email"),
        "connection": environ.get("GEOIP_AUTH_DATABASE"),
    }
    result = requests.post(
        urljoin(environ.get("GEOIP_AUTH_URL"), "/dbconnections/change_password"),
        json=jdata,
    )
    if result.status_code == 200:
        return render_template("sponsor_password_reset.html")
    else:
        return render_template("sponsor_password_reset_fail.html")
