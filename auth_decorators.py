from functools import wraps

from flask import session, redirect, url_for


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if "profile" not in session:
            return redirect(f'{url_for("login")}?origin={f.__name__}')
        return f(*args, **kwargs)

    return decorated
