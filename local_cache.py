from datetime import datetime
from functools import wraps
from collections import namedtuple
from typing import Dict

CacheItem = namedtuple("CacheItem", ["time", "value"])

_cache: Dict[str, CacheItem] = {}
# Todo: Limit cache depth


def cache_this(f):
    global _cache
    timeout = 3600

    @wraps(f)
    def wrapper(*args, **kwargs):
        args = args
        if (args in _cache) and (
            (datetime.now() - _cache[args].time).total_seconds() < timeout
        ):
            return _cache[args].value
        value = f(*args, **kwargs)
        _cache[args] = CacheItem(datetime.now(), value)
        return value

    return wrapper
