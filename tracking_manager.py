from datetime import datetime, timedelta
from os import environ
from typing import List, Dict
from urllib.parse import urljoin, quote

import requests

trackingservice_auth = None


def get_bearer():
    global trackingservice_auth
    if (trackingservice_auth is None) or (
            trackingservice_auth["expires"] - datetime.now() < timedelta(seconds=0)
    ):
        payload = {
            "grant_type": "client_credentials",
            "client_id": environ.get("GEOIP_INTERNAL_AUTH_ID"),
            "client_secret": environ.get("GEOIP_INTERNAL_AUTH_SECRET"),
            "audience": environ.get("GEOIP_TRACKINGSERVICE_URL"),
        }
        trackingservice_auth = requests.post(
            urljoin(environ.get("GEOIP_INTERNAL_AUTH_URL"), "v1.0/token"), json=payload
        ).json()
        trackingservice_auth["expires"] = datetime.now() + timedelta(seconds=trackingservice_auth["expires_in"])
    return trackingservice_auth["access_token"]


def get_pixels(user_id: str):
    url = urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/")
    url = urljoin(url, quote(user_id) + "/")
    url = urljoin(url, "pixel")
    result = requests.get(
        url, headers={"Authorization": f"Bearer {get_bearer()}"}
    )
    result.raise_for_status()
    response = []
    for pixel in result.json().get("pixels", []):
        response.append(pixel)
    return response


def create_pixel(user_id: str):
    jdata = {
        "OwnerID": user_id,
    }
    result = requests.post(
        urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), f"v1.0/pixel"),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json=jdata,
    )
    result.raise_for_status()
    return result.json()


def get_pixel(user_id: str, pixel_uid: str):
    result = requests.get(
        urljoin(
            urljoin(
                urljoin(
                    urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/"),
                    quote(user_id) + "/",
                ),
                "pixel/",
            ),
            quote(pixel_uid),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    return result.json()


def delete_pixel(user_id: str, pixel_uuid: str):
    result = requests.delete(
        urljoin(
            urljoin(
                urljoin(
                    urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/"),
                    quote(user_id) + "/",
                ),
                "pixel/",
            ),
            quote(pixel_uuid, safe=""),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    return result.json()


def get_visits(user_id: str, pixel_uuid: str) -> List[Dict]:
    result = requests.get(
        urljoin(
            urljoin(
                urljoin(
                    urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/"),
                    quote(user_id) + "/",
                ),
                "pixel/",
            ),
            quote(pixel_uuid, safe=""),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    return result.json().get("visits")


def get_tokens(user_id: str):
    url = urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/")
    url = urljoin(url, quote(user_id) + "/")
    url = urljoin(url, "token")
    result = requests.get(
        url, headers={"Authorization": f"Bearer {get_bearer()}"}
    )
    result.raise_for_status()
    response = []
    for token in result.json().get("tokens", []):
        response.append(token)
    return response


def create_token(user_id: str, application_name: str):
    jdata = {"owner_id": user_id, "application_name": application_name}
    result = requests.post(
        urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), f"v1.0/token"),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json=jdata,
    )
    result.raise_for_status()
    return result.json()


def get_token(user_id: str, token: str):
    result = requests.get(
        urljoin(
            urljoin(
                urljoin(
                    urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/"),
                    quote(user_id) + "/",
                ),
                "token/",
            ),
            quote(token, safe=""),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    if result.status_code not in [requests.codes.ok, requests.codes.not_found]:
        result.raise_for_status()
    if result.status_code == requests.codes.ok:
        return result.json()


def delete_token(user_id: str, token: str):
    result = requests.delete(
        urljoin(
            urljoin(
                urljoin(
                    urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/"),
                    quote(user_id) + "/",
                ),
                "token/",
            ),
            quote(token, safe=""),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    return result.json()


def get_token_visits(user_id: str, token: str) -> List[Dict]:
    result = requests.get(
        urljoin(
            urljoin(
                urljoin(
                    urljoin(environ.get("GEOIP_TRACKINGSERVICE_URL"), "v1.0/owner/"),
                    quote(user_id) + "/",
                ),
                "token/",
            ),
            quote(token, safe=""),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    return result.json().get("visits")
