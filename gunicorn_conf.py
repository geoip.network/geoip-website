import multiprocessing

workers = int(multiprocessing.cpu_count() * 1.5)

bind = "0.0.0.0:8080"
keepalive = 120
errorlog = "-"
pidfile = "gunicorn.pid"
