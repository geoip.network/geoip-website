from datetime import datetime
from functools import wraps, lru_cache
from os import environ
from urllib.parse import urljoin

import stripe
from flask import session, redirect, url_for

import auth0_userdata
from auth0_userdata import update_user_meta
from local_cache import cache_this

stripe.set_app_info(
    environ.get("GEOIP_STRIPE_APP_NAME"),
    version="0.0.1",
    url=environ.get("GEOIP_STRIPE_APP_URL"),
)

stripe.api_version = "2020-08-27"
stripe.api_key = environ.get("GEOIP_STRIPE_SECRET_KEY")
stripe_public_key = environ.get("GEOIP_STRIPE_PUBLIC_KEY")


def get_subscription(auth0_id: str, stripe_customer_id: str):
    active_subscription = None
    if (
        subscription_id := session.get("meta", {}).get("stripe_subscription_id")
    ) is not None:
        active_subscription = stripe.Subscription.retrieve(subscription_id)
        if active_subscription and active_subscription.status in ["canceled", "ended"]:
            active_subscription = None
    if active_subscription is None:
        clean_pricing = {price[2]: True for price in get_clean_pricing()}
        subscriptions = stripe.Subscription.list(
            customer=stripe_customer_id, status="active"
        )
        if len(subscriptions["data"]) != 0:
            for subscription_data in subscriptions["data"]:
                active_subscription = subscription_data
                line_item = active_subscription["items"]["data"][0]["price"]
                plan_details = (
                    line_item["unit_amount"],
                    line_item["nickname"],
                    line_item["id"],
                )
                if line_item["id"] in clean_pricing:
                    update_user_meta(
                        auth0_id,
                        {
                            "stripe_plan_details": plan_details,
                            "stripe_subscription_id": active_subscription.id,
                            "stripe_subscription_end": active_subscription.current_period_end,
                        },
                    )
                    session["meta"].update(
                        {
                            "stripe_plan_details": plan_details,
                            "stripe_subscription_id": active_subscription.id,
                            "stripe_subscription_end": active_subscription.current_period_end,
                        }
                    )
                    break
    return active_subscription


def get_plan_details(latest_invoice: str):
    plan = stripe.Invoice.retrieve(latest_invoice).lines.data[0].price
    plan_details = (plan["unit_amount"], plan["nickname"], plan["id"])
    return plan_details


def validate_subscription():
    subscription_end = session["meta"].get("stripe_subscription_end")
    if (subscription_end is not None) and (
        datetime.now() < datetime.fromtimestamp(subscription_end)
    ):
        return True
    elif session["meta"].get("stripe_customer_id") is not None:
        active = get_subscription(
            session["profile"]["user_id"], session["meta"].get("stripe_customer_id")
        )
        return active is not None
    return False


def requires_subscription(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        stripe_customer_id = session["meta"].get("stripe_customer_id")
        if (
            stripe_customer_id is None
            and (user_id := session.get("profile", {}).get("user_id")) is not None
        ):
            user = auth0_userdata.user_info(user_id)
            session["meta"] = user.get("app_metadata", {})
            stripe_customer_id = session["meta"].get("stripe_customer_id")
        current = None
        if stripe_customer_id is not None and validate_subscription():
            current = session["meta"].get("stripe_plan_details", (0, None, None))
        if current is None:
            return redirect(url_for("sponsor.account"))
        return f(*args, current_subscription=current, **kwargs)

    return decorated


def new_subscription(stripe_customer_id, selected):
    checkout_session = stripe.checkout.Session.create(
        customer=stripe_customer_id,
        line_items=[
            {
                "price": selected,
                "quantity": 1,
            },
        ],
        mode="subscription",
        success_url=urljoin(
            environ.get("GEOIP_STRIPE_APP_URL"), url_for("sponsor.account")
        ),
        cancel_url=urljoin(
            environ.get("GEOIP_STRIPE_APP_URL"), url_for("sponsor.account_cancel")
        ),
    )
    return redirect(checkout_session.url)


def update_subscription(active_subscription, selected):
    stripe.Subscription.modify(
        active_subscription["id"],
        items=[
            {"id": active_subscription["items"]["data"][0]["id"], "price": selected}
        ],
        proration_behavior="none",
        cancel_at_period_end=False,
    )
    return redirect(url_for("sponsor.account"))


def get_or_create_customer():
    if (stripe_customer_id := session["meta"].get("stripe_customer_id")) is None:
        customer = stripe.Customer.create(email=session["profile"]["email"])
        auth0_userdata.update_user_meta(
            session["profile"]["user_id"], {"stripe_customer_id": customer.id}
        )
        stripe_customer_id = customer.id
    return stripe_customer_id


@lru_cache(1)
def get_clean_pricing():
    clean_pricing = [
        (price["unit_amount"], price["nickname"], price["id"])
        for price in stripe.Price.list(product=environ.get("GEOIP_STRIPE_PRODUCT_ID"))
    ]
    clean_pricing.sort()
    return clean_pricing
