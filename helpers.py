from collections import defaultdict, Counter
from datetime import datetime, timedelta
from statistics import mean

import numpy as np
import pandas as pd
import plotly.express as px


def calculate_statistics(totals_histograms, visits, geoip_records):
    unique_by_country = defaultdict(set)
    for i, record in enumerate(geoip_records):
        if "error" not in record:
            unique_by_country[record["allocated_cc"]].add(visits[i]["IPAddr"])
        else:
            unique_by_country["Unknown"].add(visits[i]["IPAddr"])
        unique_by_country["Total"].add(visits[i]["IPAddr"])
    data = list()
    for country, unique_ips in unique_by_country.items():
        if country in ["Total", "Unknown"]:
            continue
        totals_histogram = totals_histograms[country]
        totals = list(totals_histogram.values())
        row = {
            "Country": country,
            "Unique Visitors": len(unique_ips),
            "Avg. Visits": round(mean(totals), 1),
            "Max. Visits": max(totals),
            "Min. Visits": max(totals),
        }
        data.append(row)
    for country in ["Unknown", "Total"]:
        unique_ips = unique_by_country[country]
        totals_histogram = totals_histograms[country]
        if len(totals_histogram) <= 0:
            continue
        totals = list(totals_histogram.values())
        row = {
            "Country": country,
            "Unique Visitors": len(unique_ips),
            "Avg. Visits": round(mean(totals), 1),
            "Max. Visits": max(totals),
            "Min. Visits": max(totals),
        }
        data.append(row)
    return data


def calculate_totals(visits, geoip_records):
    min_time = datetime.min.time()
    start_date = datetime.combine(datetime.now().date(), min_time)
    end_date = start_date - timedelta(days=30)
    current_date = end_date
    totals_histograms = defaultdict(Counter)
    while current_date <= start_date:
        for record in geoip_records:
            if "error" not in record:
                totals_histograms[record["allocated_cc"]][current_date] = 0
            else:
                totals_histograms["Unknown"][current_date] = 0
        totals_histograms["Total"][current_date] = 0
        current_date += timedelta(days=1)
    for i, visit in enumerate(visits):
        visit_date = datetime.combine(
            datetime.fromtimestamp(visit["Timestamp"]), min_time
        )
        if "error" not in geoip_records[i]:
            totals_histograms[geoip_records[i]["allocated_cc"]][visit_date] += 1
        else:
            totals_histograms["Unknown"][visit_date] += 1
        totals_histograms["Total"][visit_date] += 1
    return totals_histograms


def generate_chart(totals_histograms):
    pivot = []
    for country, data in totals_histograms.items():
        for date, total in data.items():
            pivot.append([country, date, total])
    df = pd.DataFrame(np.array(pivot), columns=["Country", "Date", "Total"])
    fig = px.line(
        df,
        x="Date",
        y="Total",
        color="Country",
        line_shape="spline",
        range_x=(min(df.Date), max(df.Date) + timedelta(days=1)),
    )
    fig.update_layout(
        yaxis_title=None, xaxis_title=None, margin=dict(l=0, r=0, b=0, t=0, pad=0)
    )
    fig.update_xaxes(
        tickangle=45, nticks=30, rangeslider_visible=True, tickformat="%Y-%m-%d"
    )
    fig.update_layout(
        legend=dict(
            orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1, title=None
        )
    )
    visits_chart = fig.to_html(config={"displayModeBar": False})
    return visits_chart
