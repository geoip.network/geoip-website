import re
from datetime import datetime, timedelta
from functools import lru_cache
from os import environ
from typing import Tuple
from urllib.parse import urljoin

import requests

from local_cache import cache_this

api_auth = None
valid_cidr = re.compile(
    r"(?P<cidr>((\d{1,3}\.){3}\d{1,3}(/\d{1,3})?)|(([0-9a-fA-F]{0,4}:){2,8}([0-9a-fA-F]{0,4})?(/\d{1,3})?))"
)


def get_bearer():
    global api_auth
    if (api_auth is None) or (
        api_auth["expires"] - datetime.now() < timedelta(seconds=0)
    ):
        payload = {
            "grant_type": "client_credentials",
            "client_id": environ.get("GEOIP_INTERNAL_AUTH_ID"),
            "client_secret": environ.get("GEOIP_INTERNAL_AUTH_SECRET"),
            "audience": environ.get("GEOIP_API_URL"),
        }
        api_auth = requests.post(
            urljoin(environ.get("GEOIP_INTERNAL_AUTH_URL"), "v1.0/token"), json=payload
        ).json()
        api_auth["expires"] = datetime.now() + timedelta(seconds=api_auth["expires_in"])
    return api_auth["access_token"]


@cache_this
def lookup_cidr(cidr_dirty: str):
    if (match := valid_cidr.search(cidr_dirty)) is None:
        return None
    cidr = match.group("cidr")
    response = requests.get(
        urljoin(environ.get("GEOIP_API_URL"), f"v1.0/cidr/{cidr}/internal"),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    jdata = None
    if 200 <= response.status_code < 300:
        jdata = response.json()
    return jdata


@lru_cache(1)
def clean_visitors(dirty_visitors):
    public_ips = []
    for cidr_dirty in dirty_visitors:
        if (match := valid_cidr.search(cidr_dirty)) is not None:
            public_ips.append(match.group("cidr"))
    return public_ips


@cache_this
def lookup_cidrs(public_ips_dirty: Tuple[str]):
    public_ips = clean_visitors(public_ips_dirty)
    response = requests.post(
        urljoin(environ.get("GEOIP_API_URL"), f"v1.0/cidrs/internal"),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json=public_ips,
    )
    jdata = []
    if 200 <= response.status_code < 300:
        jdata = response.json()
    return jdata
