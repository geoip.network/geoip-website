import sqlite3
import ipaddress
from functools import wraps
from os.path import exists

from flask import request


if not exists("../visitors.db"):
    connection = sqlite3.connect("../visitors.db")
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE visitors (ipaddress text, visits integer)")
    connection.commit()


def tracking(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if request.access_route:
            ip = request.access_route[0]
        else:
            ip = request.remote_addr
        try:
            ip = ipaddress.ip_address(ip).compressed
        except:
            ip = "0.0.0.0"
        connection = sqlite3.connect("../visitors.db")
        cursor = connection.cursor()
        try:
            cursor.execute(
                "SELECT ipaddress, visits FROM visitors WHERE ipaddress=:ip;",
                {"ip": ip},
            )
            result = cursor.fetchone()
            if result is None:
                cursor.execute("INSERT INTO visitors VALUES (?,?);", (ip, 1))
            else:
                count = result[1]
                cursor.execute(
                    "UPDATE visitors SET visits=:visits WHERE ipaddress=:ip;",
                    {"ip": ip, "visits": count + 1},
                )
        finally:
            connection.commit()
            connection.close()
        return func(*args, **kwargs, public_ip=ip)

    return wrapper


def get_tracked_ips():
    connection = sqlite3.connect("../visitors.db")
    cursor = connection.cursor()
    cursor.execute("SELECT ipaddress FROM visitors")
    results = cursor.fetchall()
    raw_result = [result[0] for result in results]
    return tuple(set(raw_result))
