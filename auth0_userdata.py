import urllib
from datetime import datetime, timedelta
from os import environ
from urllib.parse import urljoin

import requests


management_auth = None
user_info_cache = {}


def get_bearer():
    global management_auth
    if (management_auth is None) or (
        management_auth["expires"] - datetime.now() < timedelta(seconds=0)
    ):
        headers = {"content-type": "application/x-www-form-urlencoded"}
        payload = {
            "grant_type": "client_credentials",
            "client_id": environ.get("GEOIP_AUTH_ID"),
            "client_secret": environ.get("GEOIP_AUTH_SECRET"),
            "audience": urljoin(environ.get("GEOIP_AUTH_URL"), "api/v2/"),
        }
        management_auth = requests.post(
            urljoin(environ.get("GEOIP_AUTH_URL"), "oauth/token"),
            headers=headers,
            data=payload,
        ).json()
        management_auth["expires"] = datetime.now() + timedelta(
            seconds=management_auth["expires_in"]
        )
    return management_auth["access_token"]


def user_info(user_id: str):
    token = get_bearer()
    if user_id not in user_info_cache:
        user_info_cache[user_id] = requests.get(
            urljoin(
                urljoin(environ.get("GEOIP_AUTH_URL"), "api/v2/users/"),
                urllib.parse.quote(user_id),
            ),
            headers={"Authorization": f"Bearer {token}"},
        ).json()
    user_data = user_info_cache[user_id]
    return user_data


def update_user_meta(user_id: str, meta: dict):
    token = get_bearer()
    response = requests.patch(
        urljoin(
            urljoin(environ.get("GEOIP_AUTH_URL"), "api/v2/users/"),
            urllib.parse.quote(user_id),
        ),
        headers={"Authorization": f"Bearer {token}"},
        json={"app_metadata": meta},
    )
    user_info_cache[user_id] = response.json()
    return user_info_cache[user_id]
