import re

regex = (
    "((http|https)://)(www.)?"
    + "[a-zA-Z0-9@:%._\\+~#?&//=]"
    + "{2,256}\\.[a-z]"
    + "{2,6}\\b([-a-zA-Z0-9@:%"
    + "._\\+~#?&//=]*)"
)
p = re.compile(regex)


def url_safe_check(string: str) -> bool:
    return re.search(p, string) is not None
