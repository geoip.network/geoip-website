import ipaddress
import time
from os import environ, path
from urllib.parse import urlencode, urljoin
import stripe
import newrelic.agent
from authlib.integrations.flask_client import OAuth
from flask import Flask, render_template, request, session, redirect, url_for
from flask_limiter import Limiter

import api_manager
import auth_manager
import auth0_userdata
import sponsor_pages
import tracking_manager
from page_render_helpers import searchpage, build_map, render_funding_progress, mainpage
from stripe_wrapper import validate_subscription
from tracking_wrapper import tracking, get_tracked_ips


if path.isfile("./newrelic.ini"):
    newrelic.agent.initialize("./newrelic.ini")
app = Flask(__name__)
app.secret_key = environ.get("GEOIP_FLASK_SESSION_SECRET")
app.config["SPONSOR_UPLOAD_FOLDER"] = "uploads/sponsor_logos"

oauth = OAuth(app)
auth0 = oauth.register(
    "auth0",
    client_id=environ.get("GEOIP_AUTH_ID"),
    client_secret=environ.get("GEOIP_AUTH_SECRET"),
    api_base_url=environ.get("GEOIP_AUTH_URL"),
    access_token_url=environ.get("GEOIP_AUTH_TOKEN_URL"),
    authorize_url=environ.get("GEOIP_AUTH_VALIDATE_URL"),
    client_kwargs={
        "scope": "openid profile email",
    },
)

auth0_userdata.get_bearer()
auth_manager.get_bearer()
api_manager.get_bearer()
tracking_manager.get_bearer()

stripe.set_app_info(
    environ.get("GEOIP_STRIPE_APP_NAME"),
    version="0.0.1",
    url=environ.get("GEOIP_STRIPE_APP_URL"),
)

stripe.api_version = "2020-08-27"
stripe.api_key = environ.get("GEOIP_STRIPE_SECRET_KEY")
stripe_public_key = environ.get("GEOIP_STRIPE_PUBLIC_KEY")

app.register_blueprint(sponsor_pages.bp)


@app.template_filter("ctime")
def timectime(s):
    return time.ctime(s)


def get_ipaddr():  # pragma: no cover
    """
    :return: the ip address for the current request
     (or 127.0.0.1 if none found) based on the X-Forwarded-For headers.

    .. deprecated:: 0.9.2
    """
    if request.access_route:
        return request.access_route[0]
    else:
        return request.remote_addr or "127.0.0.1"


limiter = Limiter(app, key_func=get_ipaddr, default_limits=[])


@app.route("/", methods=["GET"])
@tracking
@limiter.exempt
def index(public_ip):
    return mainpage(
        "index.html",
        public_ip,
        public_ip=f"Enter a CIDR or IP e.g. {public_ip}",
    )


@app.route("/api", methods=["GET"])
@limiter.exempt
def api():
    return render_template("api.html", api_url=environ.get("GEOIP_API_URL"))


@app.route("/privacy", methods=["GET"])
@limiter.exempt
def privacy():
    return render_template("privacy.html")


@app.route("/terms-of-service", methods=["GET"])
@limiter.exempt
def tos():
    return render_template("tos.html")


@app.route("/cookie-policy", methods=["GET"])
@limiter.exempt
def cookie():
    return render_template("cookie_policy.html")


@app.route("/acceptable-usage", methods=["GET"])
@limiter.exempt
def acceptable_usage():
    return render_template("acceptable_usage.html")


@app.route("/legal", methods=["GET"])
@limiter.exempt
def legal():
    return render_template("legal.html")


@app.route("/about", methods=["GET"])
@limiter.exempt
def about():
    return render_template("about.html")


@app.route("/sponsor", methods=["GET"])
@limiter.exempt
def sponsor():
    return render_template(
        "sponsor.html", funding_progress=render_funding_progress(26.69, 3500)
    )


@app.route("/sponsor", methods=["POST"])
@limiter.limit("10 per day")
def sponsor_submit():
    return auth0.authorize_redirect(
        redirect_uri=urljoin(
            environ.get("GEOIP_STRIPE_APP_URL"), url_for("auth_callback_handling")
        ),
        screen_hint="signup",
    )


@app.route("/cidr/<path:cidr>", methods=["GET"])
@limiter.limit("200 per day")
def lookup(cidr):
    return searchpage("lookup.html", cidr, public_ip=cidr)


@app.route("/map", methods=["GET"])
@limiter.limit("1000 per day")
def map():
    if request.access_route:
        ip = request.access_route[0]
    else:
        ip = request.remote_addr
    try:
        ip = ipaddress.ip_address(ip).compressed
    except:
        ip = "0.0.0.0"
    highlight = api_manager.lookup_cidr(ip)
    geoip_records = api_manager.lookup_cidrs(get_tracked_ips())
    rendered_map = build_map(geoip_records, highlight=highlight)
    return rendered_map, 200


@app.route("/map/<path:cidr>", methods=["GET"])
@limiter.limit("500 per day")
def individual_map(cidr):
    requested = api_manager.lookup_cidr(cidr)
    geoip_records = [requested]
    rendered_map = build_map(geoip_records)
    return rendered_map, 200


@app.route("/login")
def login():
    origin = request.args.get("origin", type=str)
    session["login_origin"] = origin or "sponsor.dashboard"
    return auth0.authorize_redirect(
        redirect_uri=urljoin(
            environ.get("GEOIP_STRIPE_APP_URL"), url_for("auth_callback_handling")
        )
    )


@app.route("/auth_callback")
def auth_callback_handling():
    # Handles response from token endpoint
    auth0.authorize_access_token()
    resp = auth0.get("userinfo")
    userinfo = resp.json()
    session["profile"] = {
        "user_id": userinfo["sub"],
        "name": userinfo["name"],
        "picture": userinfo["picture"],
        "email": userinfo["email"],
    }
    user = auth0_userdata.user_info(userinfo["sub"])
    session["meta"] = user.get("app_metadata", {})
    if validate_subscription():
        origin = session.get("login_origin") or "sponsor.dashboard"
    else:
        origin = "sponsor.account"
    return redirect(url_for(origin))


@app.route("/logout")
def logout():
    # Clear session stored data
    session.clear()
    # Redirect user to logout endpoint
    params = {
        "returnTo": environ.get("GEOIP_STRIPE_APP_URL"),
        "client_id": environ.get("GEOIP_AUTH_ID"),
    }
    return redirect(f"{auth0.api_base_url}v2/logout?{urlencode(params)}")


if __name__ == "__main__":
    app.run()
