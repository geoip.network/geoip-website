from datetime import datetime, timedelta
from os import environ
from urllib.parse import urljoin, quote

import requests

authservice_auth = None


def get_bearer():
    global authservice_auth
    if (authservice_auth is None) or (
            authservice_auth["expires"] - datetime.now() < timedelta(seconds=0)
    ):
        payload = {
            "grant_type": "client_credentials",
            "client_id": environ.get("GEOIP_INTERNAL_AUTH_ID"),
            "client_secret": environ.get("GEOIP_INTERNAL_AUTH_SECRET"),
            "audience": environ.get("GEOIP_AUTHSERVICE_URL"),
        }
        authservice_auth = requests.post(
            urljoin(environ.get("GEOIP_INTERNAL_AUTH_URL"), "v1.0/token"), json=payload
        ).json()
        authservice_auth["expires"] = datetime.now() + timedelta(seconds=authservice_auth["expires_in"])
    return authservice_auth["access_token"]


def get_apikeys(user_id: str):
    result = requests.get(
        urljoin(
            urljoin(environ.get("GEOIP_AUTHSERVICE_URL"), "v1.0/dynamic/"),
            quote(user_id),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    dynamic_keys = result.json()
    result = requests.get(
        urljoin(
            urljoin(environ.get("GEOIP_AUTHSERVICE_URL"), "v1.0/bearer/"),
            quote(user_id),
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
    )
    result.raise_for_status()
    bearer_keys = result.json()
    response = []
    for key in dynamic_keys:
        dt = datetime.fromtimestamp(key["timestamp"])
        response.append(
            {
                "key_type": "Dynamic",
                "application_name": key["application_name"],
                "key_id": key["username"],
                "created_at": dt.isoformat(),
                "expires_at": dt + timedelta(seconds=31557600),
            }
        )
    for key in bearer_keys:
        dt = datetime.fromtimestamp(key["timestamp"])
        response.append(
            {
                "key_type": "Bearer",
                "application_name": key["application_name"],
                "created_at": dt.isoformat(),
                "expires_at": (dt + timedelta(seconds=31557600)).isoformat(),
            }
        )
    return response


def create_apikey(user_id: str, application_name: str, is_bearer=False):
    jdata = {
        "auth0_id": user_id,
        "application_name": application_name,
        "role": "sponsor",
        "ttl": 31557600,
    }
    result = requests.post(
        urljoin(
            environ.get("GEOIP_AUTHSERVICE_URL"),
            f"v1.0/{'bearer' if is_bearer else 'dynamic'}",
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json=jdata,
    )
    result.raise_for_status()
    return result.json()


def delete_apikey(user_id: str, application_name: str, username: str, is_bearer=False):
    jdata = {
        "auth0_id": user_id,
        "application_name": application_name,
        "username": username,
    }
    result = requests.delete(
        urljoin(
            environ.get("GEOIP_AUTHSERVICE_URL"),
            f"v1.0/{'bearer' if is_bearer else 'dynamic'}",
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json=jdata,
    )
    result.raise_for_status()
    return result.json()
