import json
from collections import OrderedDict
from datetime import datetime
from typing import Tuple

import folium
from countryinfo import CountryInfo
from flask import render_template
from geopy import Nominatim
from pycountry import languages, currencies
from pygments import formatters, highlight, lexers

import api_manager
from local_cache import cache_this

geolocator = Nominatim(user_agent="geoip.network")


def searchpage(template, ip_or_cidr, **kwargs):
    requested = api_manager.lookup_cidr(ip_or_cidr)
    if requested is None:
        result = render_template(
            template,
            information={},
            **kwargs,
        )
        return result
    stripped = stripped_requester(
        requested["allocated_cc"],
        tuple(requested["geo"]["geometry"]["coordinates"]),
        requested["geo"]["properties"]["radius"],
        requested["as-name"],
        requested["asn"],
        requested["cidr"],
        requested["rir"],
        requested["timestamp"],
    )
    formatter = formatters.HtmlFormatter(style="lovelace")
    jdata = highlight(
        json.dumps(requested, sort_keys=True, indent=4).encode(),
        lexers.JsonLexer(),
        formatter,
    )
    result = render_template(
        template,
        information=stripped,
        jdata=jdata,
        cidr=stripped["CIDR"],
        extended_css=formatter.get_style_defs(".highlight"),
        **kwargs,
    )
    return result


def mainpage(template, ip_or_cidr, **kwargs):
    requested = api_manager.lookup_cidr(ip_or_cidr)
    if requested is None:
        result = render_template(
            template,
            **kwargs,
        )
        return result
    formatter = formatters.HtmlFormatter(style="lovelace")
    jdata = highlight(
        json.dumps(requested, sort_keys=True, indent=4).encode(),
        lexers.JsonLexer(),
        formatter,
    )
    result = render_template(
        template,
        jdata=jdata,
        extended_css=formatter.get_style_defs(".highlight"),
        **kwargs,
    )
    return result


@cache_this
def stripped_requester(
    allocated_cc, coords, radius, as_name, asn, cidr, rir, timestamp
):
    stripped = OrderedDict()
    stripped["Allocated Country"] = allocated_cc
    location = geolocator.reverse((coords[1], coords[0]))
    cc = location.raw.get("address", {}).get("country_code")
    if cc:
        info = CountryInfo(cc)
        stripped["Allocated Country"] = info.name().title()
        stripped["Language"] = languages.get(alpha_2=info.languages()[0]).name
        stripped["Currency"] = currencies.get(alpha_3=info.currencies()[0]).name
    stripped["Coordinates (long, lat)"] = f"({coords[0]:.5f}, {coords[1]:.5f})"
    if radius > 0:
        stripped["Accuracy radius"] = str(radius)
    stripped["ISP"] = as_name
    stripped["ASN"] = asn
    stripped["CIDR"] = cidr
    stripped["Regional Internet Registry"] = rir
    stripped["Last updated"] = datetime.fromtimestamp(timestamp).isoformat()
    return stripped


def render_funding_progress(funded, required):
    percentage = funded / required
    green_part = round(500 * percentage, 1)
    rect1 = f'<rect width="{green_part}pt" height="25pt" style="fill:#229727" />'
    rect2 = f'<rect x="{green_part}pt" width="{(500 - green_part)}pt" height="25pt" style="fill:#972222" />'
    text1 = f'<text x="180pt" y="18pt" fill="#E5E5E5" font-family="Cutive Mono, monospace" font-weight="normal" font-size="18pt">${funded} / ${required}</text>'
    return f"{rect1}{rect2}{text1}"


def build_map(geoip_records, highlight=None):
    records = tuple(
        set(
            [
                tuple(record["geo"]["geometry"]["coordinates"])
                for record in geoip_records
                if "geo" in record
            ]
        )
    )
    highlight = (
        tuple(highlight["geo"]["geometry"]["coordinates"])
        if highlight is not None
        else None
    )
    return _build_map(records, highlight)


@cache_this
def _build_map(
    geoip_records: Tuple[Tuple[float, float], ...],
    highlight: Tuple[float, float] = None,
):
    folium_map = folium.Map(location=(0, 0), tiles="OpenStreetMap", zoom_start=1)
    folium.TileLayer("cartodbpositron").add_to(folium_map)
    for record in geoip_records:
        color = "#985ECE"
        if (highlight is not None) and (record == highlight):
            color = "#C516A5"
        folium.GeoJson(
            {
                "geometry": {"coordinates": list(record), "type": "Point"},
                "type": "Feature",
            },
            name="geojson",
            marker=folium.Circle(color=color),
        ).add_to(folium_map)
    return folium_map._repr_html_()
